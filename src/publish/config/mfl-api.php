<?php

return [

    // URLs

    'api_host' => env('MFL_API_HOST'),
    'protocol' => env('MFL_API_PROTOCOL', 'https'), // default https

    // CREDENTIALS

    'username' => env('MFL_API_USERNAME'),
    'password' => env('MFL_API_PASSWORD'),

    'api_key' => env('MFL_API_KEY'),

    'auth_method' => env('MFL_API_AUTH_TYPE', 'key'),

    // SETTINGS

    'league_id' => env('MFL_LEAGUE_ID', ''),

    'notification_mode' => [], // flash, log, or empty array for none

    'debug_mode' => false, // true - extra logging in Barryvdh Debugbar (required)

    // CACHE

    'cache_active' => env('MFL_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes' => env('MFL_API_CACHE_MINUTES', 10),
    'cache_endpoints' => [

        // league
        'league',
        'leagueStandings',

        // rosters


        // players
        'players',
    ],

];