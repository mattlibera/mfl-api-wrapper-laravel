<?php

namespace MattLibera\MflApiLaravel;

use Illuminate\Support\Facades\Cache;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class MflApi extends \MattLibera\MflApi\MflApi
{
    protected $historyMonths;

    protected $cacheActive;
    protected $endpointsToCache;
    protected $cacheMinutes;

    protected $debugMode;

    protected $authMethod;

    private $username;
    private $password;

    public function __construct()
    {

        // set vars specific to this instance of Laravel
        $this->setHost(config('mfl-api.api_host'));
        $this->setProtocol(config('mfl-api.protocol'));

        $this->authMethod = config('mfl-api.auth_method');

        if ($this->authMethod == 'key') {
            $this->setAuthKey(config('mfl-api.api_key'));
        } else {
            $this->username = config('mfl-api.username');
            $this->password = config('mfl-api.password');
        }

        if (env('HTTP_REQUEST_USE_PROXY') == 'true') {
            $this->setUseProxy(true);
            $this->setProxyHost(env('HTTP_REQUEST_PROXY_HOST'));
            $this->setProxyPort(env('HTTP_REQUEST_PROXY_PORT'));
        }

        // cache configuration
        $this->cacheActive = config('mfl-api.cache_active');
        $this->endpointsToCache = config('mfl-api.cache_endpoints');
        $this->cacheMinutes = config('mfl-api.cache_minutes');

        // debug mode (not enabled in production)
        $this->debugMode = (config('app.env') == 'production') ? false : config('mfl-api.debug_mode');
    }


    private function refreshAuthCookie()
    {
        $creds = [
            'USERNAME' => $this->username,
            'PASSWORD' => $this->password,
            'XML'      => 1 // required per documentation
        ];

        $currentHost = $this->host;
        // use API prefix
        $this->setHost('api.myfantasyleague.com');

        $authResult = $this->apiCall('post', 'misc', 'login', $creds);
        // TODO - check to be sure this was successful.
        if (isset($authResult['body']['error'])) {
            throw new \Exception('API error on authentication: ' . $authResult['body']['error']);
        } else {
            $this->userCookie = $authResult['body']['status_attr']['MFL_USER_ID'];
            session(['mfl_auth_cookie' => $this->userCookie]);
            session(['mfl_auth_cookie_created' => Carbon::now()]);
            Log::info('cookie posted to session.');
        }

        // return to previously-set host
        $this->setHost($currentHost);

        return $this->userCookie;
    }

    public function authenticate()
    {
        if ($this->authMethod == 'key') {
            Log::info('using auth key; no cookie is required.');
            return;
        }

        Log::info('not using auth key - cookie is needed.');
        if (session()->has('mfl_auth_cookie') && session()->has('mfl_auth_cookie_created')) {
            Log::info('auth cookie detected. checking validity.');
            $created = session('mfl_auth_cookie_created');

            if ($created->addSeconds(config('auth_cookie_ttl_seconds'))->lte(Carbon::now())) {
                // expired, get new
                Log::info('cookie was detected but is expired. fetching new.');
                $this->refreshAuthCookie();
            } else {
                // all good, get from session.
                Log::info('cookie was detected and is still good. using session value.');
                $this->userCookie = session('mfl_auth_cookie');
            }
        } else {
            Log::info('no session cookie was detected. fetching new.');
            $this->refreshAuthCookie();
        }
    }

    /**
     * apiCall() - performs a call to the MFL API
     *
     * @param string $method - call type (get, post, etc.)
     * @param string $command - command type (export, import, etc.)
     * @param string $type - command name (e.g. players, injuries, etc.)
     * @param array $params - arguments / options / etc. to be passed to the call
     * @param string $asUser - (optional) - the username with which to perform the API call, if not the commissioner
     *
     * @return array
     */

    protected function apiCall($method, $command, $type, $params = [], $authMethod = 'key', $asUser = null)
    {
        if ($this->authMethod != 'key' && $type != 'login' && empty($this->userCookie)) {
            Log::info('need auth cookie. evaluating...');
            $this->authenticate();
        }

        $hashedCacheKey = hash('sha256', $method . $command . $type . json_encode($params) . $asUser);
        $this->debugMessage('Hashed cache key: ' . $hashedCacheKey);

        if ($this->cacheActive && Cache::has($hashedCacheKey)) {
            // in cache
            $resultArray = Cache::get($hashedCacheKey);
            $returnArray = [
                'source'  => 'cache',
                'created' => Cache::get($hashedCacheKey . '-created', 'unknown')
            ];
            $message = 'API data for \'' . $type . '\' retrieved from cache (created: ' . $returnArray['created'] . ')';
            $messageLevel = 'success';
        } else {
            // not in cache or cache inactive
            $this->debugStartMeasure('apiCall', 'Making API call to MFL');
            $resultArray = parent::apiCall($method, $command, $type, $params, $this->authMethod, $asUser);
            $this->debugStopMeasure('apiCall');

            $created = time();

            // cache if the call was successful, and we are supposed to cache it...
            if ($resultArray['response']['httpCode'] == 200) {
                $messageLevel = 'success';
                $message = 'API call to \'' . $type . '\' successful.';

                // TODO - here
                if ($this->cacheActive && strtolower($method) == 'get') {
                    if (in_array($type, $this->endpointsToCache)) {
                        Cache::put($hashedCacheKey, $resultArray, now()->addMinutes($this->cacheMinutes));
                        Cache::put($hashedCacheKey . '-created', $created, now()->addMinutes($this->cacheMinutes));
                        $message .= ' Result cached in API cache.';
                    } else {
                        $message .= ' Not a cacheable API call.';
                    }
                }
            } else {
                $message = 'API error calling \'' . $type . '\': ' . $resultArray['response']['httpCode'] . ' - ' . $resultArray['response']['httpReason'] . ' (' . $resultArray['response']['message'] . ')';
                $messageLevel = 'danger';
            }
            $returnArray = [
                'source'  => 'api',
                'created' => $created
            ];
        }

        // handle notifications / logging / etc.
        $notificationMode = config('mfl-api.notification_mode');

        if (in_array('flash', $notificationMode)) {
            flash($message, $messageLevel);
        }

        if (in_array('log', $notificationMode)) {
            if ($messageLevel == 'danger') {
                Log::error($message);
            } else {
                Log::info($message);
            }
        }

        $finalArray = array_merge($returnArray, $resultArray);

        $this->debugInfo($finalArray);

        return array_merge($finalArray);
    }

    // debug helpers

    protected function debugMessage($message, $label = null)
    {
        if ($this->debugMode) {
            if (!is_null($label)) {
                Debugbar::addMessage($message, $label);
            } else {
                Debugbar::addMessage($message);
            }
        }
    }

    protected function debugInfo($object)
    {
        if ($this->debugMode) {
            Debugbar::info($object);
        }
    }

    protected function debugStartMeasure($key, $label = '')
    {
        if ($this->debugMode) {
            Debugbar::startMeasure($key, $label);
        }
    }

    protected function debugStopMeasure($key)
    {
        if ($this->debugMode) {
            Debugbar::stopMeasure($key);
        }
    }
}
