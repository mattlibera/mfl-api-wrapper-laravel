# MFL (MyFantasyLeague) API Library - Laravel Wrapper

Contact: matt@mattlibera.com

# Introduction

This package is a Laravel wrapper for the MFL API PHP Library package, so that the MFL API PHP Library can be used in Laravel apps.

> This is a **work in progress**. Not recommend for production apps just yet.

---

# Installation

1. `composer require 'mattlibera/mfl-api-wrapper-laravel'`
2. **IF RUNNING LARAVEL 5.5+, you may skip this step, as automatic package discovery will pick this up.** For Laravel 5.4 and below, add `MattLibera\MflApiLaravel\ServiceProvider::class,` to your `config/app.php` file manually.
3. Run `php artisan vendor:publish --provider='MattLibera\MflApiLaravel\ServiceProvider` - to publish the `mfl-api.php` config file
4. Set your environment credentials in your `.env` file, and set your configuration options in `config/mfl-api.php`

> Note: do not use protocols (http:// or https://) for `MFL_API_HOST`. Instead put your protocol (preferably https), letters only, into `MFL_API_PROTOCOL`

```
MFL_API_PROTOCOL=
MFL_API_HOST=

MFL_API_USERNAME=
MFL_API_PASSWORD=

MFL_API_KEY=
```

Note that you do not need to use both the USERNAME/PASSWORD combination and the API KEY combination; you can use either one. If your app will allow others to log into MFL to take actions on leagues in their account, then you'll probably use the former. If the app will primarily be commissioner-centric, then the latter is probably fine. You can get your API key by logging into a league and then going to Help > Developer API in the menu.

## Dependencies

This package has dependencies on `mattlibera/mfl-api-php-library` and `barryvdh/laravel-debugbar` (dev)

---

# Usage

## Basic Usage / Getting Started

In your code, assuming you've set your information/credentials properly in your `.env` file, you should be able to instantiate the `MattLibera\MflApiLaravel\MflApi` class, and then use any of its available methods (inherited from `MattLibera\MflApi\MflApi`) to make an API call.

## Configuration options

### Notification Mode

There are two notification options built into this library. You may flash the information on the screen as a flash message, or log it in the Laravel logs (whatever you've got set).

In the `config/mfl-api.php` file, `notification_mode` is an array. Inside, add one or both of `flash` and `log`.

If you choose to roll this into a higher-level custom notification / logging system for your app, you can leave this array empty to disable both of these methods, and then handle your own logging based on the return array from the API call (the API class returns the entire response, including response code, from every call).

> As of version 0.2, no flash messaging package is included. You are free to install your own - recommend `laracasts/flash` or something that implements a similar API.

### Debug Mode

_Requires DebugBar by Barryvdh (`barryvdh\laravel-debugbar`)_

If you turn on Debug Mode in the `config/mfl-api.php` file, extra information about the call will be written to the Debugbar. This includes timing, hashed cache key, and full results from each call.

 > Note: in accordance with best practices for this package, this will only work if the app environment is not set to `production`

## Caching

For speed, adherence to rate limits, and overall performance, API call caching is built into this library. This utilizes the Laravel Cache mechanism (which, like Logs, can be set however you want in the Laravel config). You can optionally disable this by adding `MFL_API_CACHING=off` in your `.env` file.

This caching isn't fancy; it'll literally just cache the result body from the API call. Depending on what your app is going to do, you may want to take this a step further and cache into some database tables. But this is here for you, for whatever that's worth to you.

### Cache TTL

The TTL of the cache is set to 10 minutes by default, but by adding `MFL_API_CACHE_MINUTES=x` to your `.env` file, you can set the cache to expire after x minutes.

### Cached endpoints

Caching is performed only for specific endpoints (e.g. requests that would equate to typical `GET` requests). Obviously, you would not want to cache requests that are designed to actually modify information in MFL (equivalent to `POST` or `PATCH` or `DELETE` requests).

By default, there is a basic set of endpoints set up in `config/mfl-api.php`. If you would like change which endpoints are / are not cached (or add your own), simply modify the `cached_endpoints` array in the config file.

In MFL, an "endpoint" can be structured differently depending on the operation. In this case, we're only going to cache the 'export' operations, and so you can assume that anything listed in `cached_endpoints` is a command under the 'export' operation set.

---

# Version History

## 0.3

- Fix for authentication so that we use the `api` prefix for the auth call but then reset the host back to whatever is in the config.

## 0.2.1

- PSR4

## 0.2

- get rid of `standaniels/flash` library dependency

## 0.1

- First real release.
- `.env` integration for MFL credentials
- Debug mode
- Caching
